const observer = new MutationObserver((mutations, me) => {
  const toggle = document.querySelector('paper-toggle-button#toggle');

  if (!toggle) {
    return;
  }

  const autoplayEnabled = toggle.getAttribute('aria-pressed');

  if (autoplayEnabled === null) {
    return;
  }

  if (autoplayEnabled === 'false') {
    observer.disconnect();
    return;
  }

  const toggleButton = document.querySelector('#toggleButton');

  if (!toggleButton) {
    return;
  }

  toggleButton.click();
  observer.disconnect();
});

observer.observe(document, {
  childList: true,
  subtree: true
});
